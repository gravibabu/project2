package com.bepoc.PhcVol;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PhcVol {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String accountName;
    private String accountTrack;
    private String accountDu;
    private String dm;
    private String da;
    private double phcTarget;
    private double volTarget;
    private double phcAOD;
    private double volAOD;
    private double phcAOL;
    private double volAOL;
    private String remarks;
    private boolean showTextbox;
    private boolean showUpdatebutton;

    public PhcVol() {
    }

    public PhcVol(int id, String accountName, String accountTrack, String accountDu, String dm, String da, double phcTarget, double volTarget, double phcAOD, double volAOD, double phcAOL, double volAOL, String remarks, boolean showTextbox,boolean showUpdatebutton) {
        this.id = id;
        this.accountName = accountName;
        this.accountTrack = accountTrack;
        this.accountDu= accountDu;
        this.dm = dm;
        this.da = da;
        this.phcTarget = phcTarget;
        this.volTarget = volTarget;
        this.phcAOD = phcAOD;
        this.volAOD = volAOD;
        this.phcAOL = phcAOL;
        this.volAOL = volAOL;
        this.remarks = remarks;
        this.showTextbox = showTextbox;
        this.showUpdatebutton = showUpdatebutton;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountTrack() {
        return accountTrack;
    }

    public void setAccountTrack(String accountTrack) {
        this.accountTrack = accountTrack;
    }

    public String getAccountDu() {
        return accountDu;
    }

    public void setAccountDu(String accountDu) {
        this.accountDu = accountDu;
    }

    public String getDm() {
        return dm;
    }

    public void setDm(String dm) {
        this.dm = dm;
    }

    public String getDa() {
        return da;
    }

    public void setDa(String da) {
        this.da = da;
    }

    public double getPhcTarget() {
        return phcTarget;
    }

    public void setPhcTarget(double phcTarget) {
        this.phcTarget = phcTarget;
    }

    public double getVolTarget() {
        return volTarget;
    }

    public void setVolTarget(double volTarget) {
        this.volTarget = volTarget;
    }

    public double getPhcAOD() {
        return phcAOD;
    }

    public void setPhcAOD(double phcAOD) {
        this.phcAOD = phcAOD;
    }

    public double getVolAOD() {
        return volAOD;
    }

    public void setVolAOD(double volAOD) {
        this.volAOD = volAOD;
    }

    public double getPhcAOL() {
        return phcAOL;
    }

    public void setPhcAOL(double phcAOL) {
        this.phcAOL = phcAOL;
    }

    public double getVolAOL() {
        return volAOL;
    }

    public void setVolAOL(double volAOL) {
        this.volAOL = volAOL;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isShowTextbox() {
        return showTextbox;
    }

    public void setShowTextbox(boolean showTextbox) {
        this.showTextbox = showTextbox;
    }

    public boolean isShowUpdatebutton() {
        return showUpdatebutton;
    }

    public void setShowUpdatebutton(boolean showUpdatebutton) {
        this.showUpdatebutton = showUpdatebutton;
    }
}
